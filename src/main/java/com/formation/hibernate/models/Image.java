package com.formation.hibernate.models;

import javax.persistence.*;

@Entity
public class Image {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long num;
	private Integer largeur;
	private Integer longueur;
	@Column(nullable = false)
	private String nom;
	@Column(columnDefinition = "TEXT", nullable = false)
	private String lien;
	@ManyToOne
	@JoinColumn(nullable = false)
	private Article article;

	public Image(Integer largeur, Integer longueur, String nom, String lien, Article article) {
		this.largeur = largeur;
		this.longueur = longueur;
		this.nom = nom;
		this.lien = lien;
		this.article = article;
	}

	public Image() {

	}

	@Override
	public String toString() {
		return String.format(
				"%n- Image num : %s%nNom : %s%nLongueur : %d%nLargeur : %d%nLien : %s%nArticle lié : %s - Réf : %s%n",
				num, nom, longueur, largeur, lien, article.getNom(), article.getRef());
	}

	public Long getNum() {
		return num;
	}

	public void setNum(Long num) {
		this.num = num;
	}

	public Integer getLargeur() {
		return largeur;
	}

	public void setLargeur(Integer largeur) {
		this.largeur = largeur;
	}

	public Integer getLongueur() {
		return longueur;
	}

	public void setLongueur(Integer longueur) {
		this.longueur = longueur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLien() {
		return lien;
	}

	public void setLien(String lien) {
		this.lien = lien;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

}
