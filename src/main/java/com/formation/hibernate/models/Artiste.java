package com.formation.hibernate.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Artiste {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String nom;
	@Column(nullable = false)
	private String prenom;
	@Column(nullable = false)
	private String email;
	private String nationalite;
	@ManyToMany
	@JoinTable(name = "jointure_artiste_article", joinColumns = @JoinColumn(name = "artiste_id"), inverseJoinColumns = @JoinColumn(name = "article_id"))
	private List<Article> articles = new ArrayList<>();;

	public Artiste(String nom, String prenom, String email, String nationalite) {
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.nationalite = nationalite;
	}

	public Artiste() {

	}

	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(String.format(
				"%n- Artiste id : %s%nNom : %s%n Prénom : %s%nNationalité : %s%nE-mail : %s%nArticle(s) liée(s) :%n", id,
				nom, prenom, nationalite, email));
		articles.forEach(art -> bld.append(String.format("%-20s - réf : %s%n", art.getNom(), art.getRef())));
		return bld.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

}
