package com.formation.hibernate.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Article {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ref;
	@Column(nullable = false, length = 100)
	private String nom;
	@Column(columnDefinition = "TEXT")
	private String description;
	@Column(nullable = false)
	private Double prix;
	@OneToMany(mappedBy = "article")
	private List<Image> images = new ArrayList<>();
	@ManyToMany
	@JoinTable(name = "jointure_artiste_article", joinColumns = @JoinColumn(name = "article_id"), inverseJoinColumns = @JoinColumn(name = "artiste_id"))
	private List<Artiste> artistes = new ArrayList<>();

	public Article(String nom, String description, Double prix) {
		this.nom = nom;
		this.description = description;
		this.prix = prix;
	}

	public Article() {

	}

	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(String.format("%n- Article réf : %s%nNom : %s%nPrix : %.2f%nDescription : %s%nImage(s) liée(s) :%n",
				ref, nom, prix, description));
		images.forEach(img -> bld.append(String.format("%-20s - %s%n", img.getNom(), img.getLien())));
		bld.append("Artiste(s) lié(s) : \n");
		artistes.forEach(art -> bld.append(String.format("%-20s %s%n", art.getPrenom(), art.getNom())));
		return bld.toString();
	}

	public Long getRef() {
		return ref;
	}

	public void setRef(Long ref) {
		this.ref = ref;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public List<Artiste> getArtistes() {
		return artistes;
	}

	public void setArtistes(List<Artiste> artistes) {
		this.artistes = artistes;
	}

}
