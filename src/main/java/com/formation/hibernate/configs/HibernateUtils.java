package com.formation.hibernate.configs;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {

	private static Session session = buildSessionFactory().openSession();

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static Session getSession() {
		try {
			if (!session.isOpen()) {
				session = buildSessionFactory().openSession();
				return session;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			return session;
		}
		return session;

	}

	public static void rollbackTransaction(Transaction ts) {
		if (ts != null) {
			try {
				ts.rollback();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private HibernateUtils() {
	}

}
