package com.formation.hibernate.services;

import java.util.Collection;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.formation.hibernate.configs.HibernateUtils;
import com.formation.hibernate.models.Article;
import com.formation.hibernate.repositories.ArticleRepository;

public class ArticleService implements ArticleRepository {

	private final Session session = HibernateUtils.getSession();

	@Override
	public Optional<Collection<Article>> findAll() {
		try {
			return Optional.ofNullable(session.createQuery("from Article", Article.class).list());
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public Optional<Article> findById(Long id) {
		try {
			return Optional.ofNullable(session.find(Article.class, id));
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public int save(Article art) {

		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			session.saveOrUpdate(art);
			ts.commit();
			return 1;

		} catch (Exception e) {
			HibernateUtils.rollbackTransaction(ts);
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int delete(Article art) {

		Transaction ts = null;
		try {
			if (!(art.getImages().isEmpty())) {
				ImageService is = new ImageService();
				art.getImages().forEach(img -> is.findById(img.getNum()).ifPresent(is::delete));
			}
			ts = session.beginTransaction();
			session.delete(art);
			ts.commit();
			return 1;

		} catch (Exception e) {
			HibernateUtils.rollbackTransaction(ts);
			e.printStackTrace();
			return 0;
		}
	}

}
