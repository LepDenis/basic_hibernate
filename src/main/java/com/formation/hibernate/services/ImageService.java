package com.formation.hibernate.services;

import java.util.Collection;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.formation.hibernate.configs.HibernateUtils;
import com.formation.hibernate.models.Image;
import com.formation.hibernate.repositories.ImageRepository;

public class ImageService implements ImageRepository {

	private final Session session = HibernateUtils.getSession();

	@Override
	public Optional<Collection<Image>> findAll() {
		try {
			return Optional.ofNullable(session.createQuery("from Image", Image.class).list());
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public Optional<Image> findById(Long id) {
		try {
			return Optional.ofNullable(session.find(Image.class, id));
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public int save(Image img) {

		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			session.saveOrUpdate(img);
			ts.commit();
			return 1;

		} catch (Exception e) {
			HibernateUtils.rollbackTransaction(ts);
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int delete(Image img) {

		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			session.delete(img);
			ts.commit();
			return 1;

		} catch (Exception e) {
			HibernateUtils.rollbackTransaction(ts);
			e.printStackTrace();
			return 0;
		}
	}

}
