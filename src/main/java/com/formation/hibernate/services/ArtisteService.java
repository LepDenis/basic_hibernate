package com.formation.hibernate.services;

import java.util.Collection;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.formation.hibernate.configs.HibernateUtils;
import com.formation.hibernate.models.Artiste;
import com.formation.hibernate.repositories.ArtisteRepository;

public class ArtisteService implements ArtisteRepository {

	private final Session session = HibernateUtils.getSession();

	@Override
	public Optional<Collection<Artiste>> findAll() {
		try {
			return Optional.ofNullable(session.createQuery("from Artiste", Artiste.class).list());
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public Optional<Artiste> findById(Long id) {
		try {
			return Optional.ofNullable(session.find(Artiste.class, id));
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public int save(Artiste art) {

		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			session.saveOrUpdate(art);
			ts.commit();
			return 1;

		} catch (Exception e) {
			HibernateUtils.rollbackTransaction(ts);
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int delete(Artiste art) {

		Transaction ts = null;
		try {
			art.getArticles().forEach(article -> {
				if (article.getArtistes().size() == 1) {
					ArticleService as = new ArticleService();
					as.findById(article.getRef()).ifPresent(as::delete);
				}
			});
			ts = session.beginTransaction();
			session.delete(art);
			ts.commit();
			return 1;

		} catch (Exception e) {
			HibernateUtils.rollbackTransaction(ts);
			e.printStackTrace();
			return 0;
		}
	}

}
