package com.formation.hibernate;

import org.hibernate.Session;

import com.formation.hibernate.configs.HibernateUtils;
import com.formation.hibernate.models.Article;
import com.formation.hibernate.models.Artiste;
import com.formation.hibernate.models.Image;
import com.formation.hibernate.services.ArticleService;
import com.formation.hibernate.services.ArtisteService;
import com.formation.hibernate.services.ImageService;

public class Driver {

	public static void main(String[] args) {

		ImageService imgServ = new ImageService();
		ArticleService articleServ = new ArticleService();
		ArtisteService artisteServ = new ArtisteService();

//		creerDbEtObjets(articleServ, artisteServ, imgServ);

//		System.out.println("\n---- Test find all images ----\n");
//		imgServ.findAll().stream().forEach(System.out::println);
//
//		System.out.println("\n---- Test find all articles ----\n");
//		articleServ.findAll().stream().forEach(System.out::println);
//
//		System.out.println("\n---- Test find all artistes ----\n");
//		artisteServ.findAll().stream().forEach(System.out::println);

//		artisteServ.findById(2L).ifPresent(artisteServ::delete);

	}

	private static void creerDbEtObjets(ArticleService articleServ, ArtisteService artisteServ, ImageService imgServ) {

		Article article1 = new Article("Nike Air Max", "Une belle paire de chaussure, qu'on est bien dedans !", 100D);
		Article article2 = new Article("Article 2", "Descript de l'article 2 !!", 45.0);
		Article article3 = new Article("Article 3", "Description au pifomètre...", 66.59);
		Article article4 = new Article("Article 4", "Je sais vraiment pas quoi écrire", 88.50);
		Article article5 = new Article("Article 5", "Desc qui n'a aucun sens !", 120.0);
		Article article6 = new Article("Article 6", "Et une dernière pour la route", 225.95);

		Artiste artiste1 = new Artiste("nom_artiste1", "prenom_artiste1", "artiste1@artisteland.com", "Allemagne");
		Artiste artiste2 = new Artiste("nom_artiste2", "prenom_artiste2", "artiste2@artistecountry.com", "Venezuela");
		Artiste artiste3 = new Artiste("nom_artiste3", "prenom_artiste3", "artiste3@artistenation.com", "Japon");
		Artiste artiste4 = new Artiste("nom_artiste4", "prenom_artiste4", "artiste4@artisteworld.com", "Ghana");

		Image img101 = new Image(1080, 1920, "img101", "src/images/image101.jpg", article1);
		Image img102 = new Image(1080, 1920, "img102", "src/images/image102.jpg", article1);
		Image img103 = new Image(1080, 1920, "img103", "src/images/image103.jpg", article1);
		Image img104 = new Image(1080, 1920, "img104", "src/images/image104.jpg", article1);

		Image img201 = new Image(1080, 1920, "img201", "src/images/image201.jpg", article2);
		Image img202 = new Image(1080, 1920, "img202", "src/images/image202.jpg", article2);

		Image img301 = new Image(1080, 1920, "img301", "src/images/image301.jpg", article3);
		Image img302 = new Image(1080, 1920, "img302", "src/images/image302.jpg", article3);
		Image img303 = new Image(1080, 1920, "img303", "src/images/image303.jpg", article3);

		Image img401 = new Image(1080, 1920, "img401", "src/images/image401.jpg", article4);
		Image img402 = new Image(1080, 1920, "img402", "src/images/image402.jpg", article4);

		Image img501 = new Image(1080, 1920, "img501", "src/images/image501.jpg", article5);

		article1.getArtistes().add(artiste1);
		article2.getArtistes().add(artiste2);
		article3.getArtistes().add(artiste3);
		article4.getArtistes().add(artiste4);
		article4.getArtistes().add(artiste2);
		article5.getArtistes().add(artiste2);
		article5.getArtistes().add(artiste3);
		article5.getArtistes().add(artiste4);
		article6.getArtistes().add(artiste3);

		Session session = HibernateUtils.getSession();

		System.out.println("\n----------- Sauvegarde des artistes ----------\n");

		artisteServ.save(artiste1);
		artisteServ.save(artiste2);
		artisteServ.save(artiste3);
		artisteServ.save(artiste4);

		System.out.println("\n---------- Sauvegarde des articles ----------\n");

		articleServ.save(article1);
		articleServ.save(article2);
		articleServ.save(article3);
		articleServ.save(article4);
		articleServ.save(article5);
		articleServ.save(article6);

		System.out.println("\n---------- Sauvegarde des images ----------\n");

		imgServ.save(img101);
		imgServ.save(img102);
		imgServ.save(img103);
		imgServ.save(img104);

		imgServ.save(img201);
		imgServ.save(img202);

		imgServ.save(img301);
		imgServ.save(img302);
		imgServ.save(img303);

		imgServ.save(img401);
		imgServ.save(img402);

		imgServ.save(img501);

		session.close();
	}

}
