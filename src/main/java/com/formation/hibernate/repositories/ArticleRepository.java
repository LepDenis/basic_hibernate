package com.formation.hibernate.repositories;

import java.util.Collection;
import java.util.Optional;

import com.formation.hibernate.models.Article;

public interface ArticleRepository extends CrudRepository<Article, Long> {

	Optional<Collection<Article>> findAll();

	Optional<Article> findById(Long id);

	int save(Article m);

	int delete(Article m);

}
