package com.formation.hibernate.repositories;

import java.util.Collection;
import java.util.Optional;

public interface CrudRepository<M, I> {

	Optional<Collection<M>> findAll();
	
	Optional<M> findById(I id);
	
	int save(M m);
	
	int delete(M m);
}
