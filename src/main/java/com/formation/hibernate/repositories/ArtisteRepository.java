package com.formation.hibernate.repositories;

import java.util.Collection;
import java.util.Optional;

import com.formation.hibernate.models.Artiste;

public interface ArtisteRepository extends CrudRepository<Artiste, Long> {

	Optional<Collection<Artiste>> findAll();

	Optional<Artiste> findById(Long id);

	int save(Artiste m);

	int delete(Artiste m);

}
