package com.formation.hibernate.repositories;

import java.util.Collection;
import java.util.Optional;

import com.formation.hibernate.models.Image;

public interface ImageRepository extends CrudRepository<Image, Long> {

	Optional<Collection<Image>> findAll();

	Optional<Image> findById(Long id);

	int save(Image m);

	int delete(Image m);

}
